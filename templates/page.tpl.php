<?php
/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/garland.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_right']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['header']: Items for the header region.
 * - $page['footer']: Items for the footer region.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 */
 //print_r($node);
 //print $node->uid;

?>

<header id="header" class="clearfix">
  <div class="header">
  	<div id="site-logo">
   <?php if ($logo): ?>
      <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo"><img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" /></a>
    <?php endif; ?>

    <?php if ($site_name || $site_slogan): ?>
      <div id="name-and-slogan">
        <?php if ($site_name): ?>
          <?php if ($title): ?>
            <div id="site-name"><strong>
              <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><span><?php print $site_name; ?></span></a>
            </strong></div>
          <?php else: /* Use h1 when the content title is empty */ ?>
            <h1 id="site-name">
              <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><span><?php print $site_name; ?></span></a>
            </h1>
          <?php endif; ?>
        <?php endif; ?>

        <?php if ($site_slogan): ?>
          <div id="site-slogan"><?php print $site_slogan; ?></div>
        <?php endif; ?>
      </div><!-- /#name-and-slogan -->
    <?php endif; ?>
	</div>
    <nav id="navigation" role="navigation">
      <div id="main-menu">
        <?php 
          if (module_exists('i18n_menu')) {
            $main_menu_tree = i18n_menu_translated_tree(variable_get('menu_main_links_source', 'main-menu'));
          } else {
            $main_menu_tree = menu_tree(variable_get('menu_main_links_source', 'main-menu'));
          }
          print drupal_render($main_menu_tree);
        ?>
      </div>
    </nav>
    <!--About Blogger -->
    <section id="featured" style="clear:left;">
      <div style="position: relative;" id="slides">
        <?php //if ($is_front): ?>
        <?php if (theme_get_setting('slideshow_display','responsive_personal_blog')): ?>
        <?php 
        $slide1_url = check_plain(theme_get_setting('slide1_url','responsive_personal_blog'));
        $slide2_url = check_plain(theme_get_setting('slide2_url','responsive_personal_blog'));
        $slide3_url = check_plain(theme_get_setting('slide3_url','responsive_personal_blog'));
        $slide4_url = check_plain(theme_get_setting('slide4_url','responsive_personal_blog'));
        $slide1_desc = check_markup(theme_get_setting('slide1_desc', 'responsive_personal_blog'), 'full_html'); 
        $slide2_desc = check_markup(theme_get_setting('slide2_desc', 'responsive_personal_blog'), 'full_html'); 
        $slide3_desc = check_markup(theme_get_setting('slide3_desc', 'responsive_personal_blog'), 'full_html'); 
        $slide4_desc = check_markup(theme_get_setting('slide4_desc', 'responsive_personal_blog'), 'full_html'); 
        ?>
        <section style="position: absolute; top: 0px; left: 0px; display: none; z-index: 4; opacity: 0; " id="slide-1"> <img src="<?php print base_path() . drupal_get_path('theme', 'responsive_personal_blog') . '/images/slide-01.png'; ?>" class="pngfix"/>
            <?php if($slide1_desc) { print '<p class="featured-text">' . $slide1_desc . '</p>'; } ?>
          <p class="readmore"><a href="<?php print url($slide1_url); ?>">Read More ></a></p>
        </section>
        <section style="position: absolute; top: 0px; left: 0px; display: none; z-index: 4; opacity: 0; " id="slide-2"> <img src="<?php print base_path() . drupal_get_path('theme', 'responsive_personal_blog') . '/images/slide-02.png'; ?>" class="pngfix"/>
            <?php if($slide2_desc) { print '<p class="featured-text">' . $slide2_desc . '</p>'; } ?>
          <p class="readmore"><a href="<?php print url($slide2_url); ?>">Read More ></a></p>
        </section>
        <section style="position: absolute; top: 0px; left: 0px; display: block; z-index: 5; opacity: 1; " id="slide-3"> <img src="<?php print base_path() . drupal_get_path('theme', 'responsive_personal_blog') . '/images/slide-03.png'; ?>" class="pngfix"/>
            <?php if($slide3_desc) { print '<p class="featured-text">' . $slide3_desc . '</p>'; } ?>
          <p class="readmore"><a href="<?php print url($slide3_url); ?>">Read More ></a></p>
        </section>
        <section style="position: absolute; top: 0px; left: 0px; display: none; z-index: 4; opacity: 0; " id="slide-4"> <img src="<?php print base_path() . drupal_get_path('theme', 'responsive_personal_blog') . '/images/slide-04.png'; ?>" class="pngfix"/>
            <?php if($slide4_desc) { print '<p class="featured-text">' . $slide4_desc . '</p>'; } ?>
          <p class="readmore"><a href="<?php print url($slide4_url); ?>">Read More ></a></p>
        </section>
        <?php endif; ?>
        <?php //endif; ?>
      </div>
      <nav id="nav">
        <ul>
          <li class="" id="feature-1"><a href="#">&nbsp;</a></li>
          <li class="" id="feature-2"><a href="#">&nbsp;</a></li>
          <li class="activeSlide" id="feature-3"><a href="#">&nbsp;</a></li>
          <li class="" id="feature-4"><a href="#">&nbsp;</a></li>
        </ul>
      </nav>
    </section>
  </div>
  <!--About Blogger End-->
  </div>
</header>
<div id="wrapper">
  <div id="preface-area" class="clearfix">
    <?php if($page['preface_first'] || $page['preface_middle'] || $page['preface_last']) : ?>
    <div id="preface-block-wrap" class="clearfix in<?php print (bool) $page['preface_first'] + (bool) $page['preface_middle'] + (bool) $page['preface_last']; ?>">
      <?php if($page['preface_first']): ?>
      <div class="preface-block"> <?php print render ($page['preface_first']); ?> </div>
      <?php endif; ?>
      <?php if($page['preface_middle']): ?>
      <div class="preface-block"> <?php print render ($page['preface_middle']); ?> </div>
      <?php endif; ?>
      <?php if($page['preface_last']): ?>
      <div class="preface-block"> <?php print render ($page['preface_last']); ?> </div>
      <?php endif; ?>
    </div>
    <?php endif; ?>
    <?php print render($page['header']); ?> </div>
  <div id="main" class="clearfix">
    <div id="primary">
      <section id="content" role="main">
        <?php if (theme_get_setting('breadcrumbs')): ?>
        <?php if ($breadcrumb): ?>
        <div id="breadcrumbs"><?php print $breadcrumb; ?></div>
        <?php endif;?>
        <?php endif; ?>
        <?php print $messages; ?>
        <?php if ($page['content_top']): ?>
        <div id="content_top"><?php print render($page['content_top']); ?></div>
        <?php endif; ?>
        <div id="content-wrap"> <?php print render($title_prefix); ?>
          <?php if ($title): ?>
          <h1 class="title" id="page-title"><?php print $title; ?></h1>
          <?php endif; ?>
          <?php print render($title_suffix); ?>
          <?php if (!empty($tabs['#primary'])): ?>
          <div class="tabs-wrapper clearfix"><?php print render($tabs); ?></div>
          <?php endif; ?>
          <?php print render($page['help']); ?>
          <?php if ($action_links): ?>
          <ul class="action-links">
            <?php print render($action_links); ?>
          </ul>
          <?php endif; ?>
          <?php print render($page['content']); ?> </div>
      </section>
      <!-- /#main -->
    </div>
    <?php if ($page['sidebar_right']): ?>
    <aside id="sidebar" role="complementary">
      
      <?php print render($page['sidebar_right']); ?> 
	  <?php if (theme_get_setting('socialicon_display', 'responsive_personal_blog')): ?>
      <?php 
        $twitter_url = theme_get_setting('twitter_url', 'responsive_personal_blog'); 
        $facebook_url = theme_get_setting('facebook_url', 'responsive_personal_blog'); 
        $delicious_url = theme_get_setting('delicious_url', 'responsive_personal_blog'); 
        $pinterest_url = theme_get_setting('pinterest_url', 'responsive_personal_blog');
        ?>
      <div class="social-profile block ">
	  	<h2>Be Social</h2>
        <div class="content">
          <ul>
            <?php if ($facebook_url): ?>
            <li class="facebook"> <a target="_blank" title="<?php print $site_name; ?> in Facebook" href="<?php print $facebook_url; ?>">Facebook </a> </li>
            <?php endif; ?>
            <?php if ($twitter_url): ?>
            <li class="twitter"> <a target="_blank" title="<?php print $site_name; ?> in Twitter" href="<?php print $twitter_url; ?>"> Twitter </a> </li>
            <?php endif; ?>
            <?php if ($delicious_url): ?>
            <li class="delicious"> <a target="_blank" title="<?php print $site_name; ?> in Delicious" href="<?php print $delicious_url; ?>">Delicious </a> </li>
            <?php endif; ?>
            <?php if ($pinterest_url): ?>
            <li class="pinterest"> <a target="_blank" title="<?php print $site_name; ?> in Pinterest" href="<?php print $pinterest_url; ?>">Pinterest  </a> </li>
            <?php endif; ?>
            <li class="rss"> <a target="_blank" title="<?php print $site_name; ?> in RSS" href="<?php print $front_page; ?>rss.xml"> RSS </a> </li>
          </ul>
        </div>
      </div>
      <?php endif; ?>
	  </aside>
    <?php endif; ?>
  </div>
</div>
<footer id="footer-bottom">
  <div id="footer-area" class="clearfix">
    <?php if ($page['footer_first'] || $page['footer_second'] || $page['footer_third']): ?>
    <div id="footer-block-wrap" class="clearfix in<?php print (bool) $page['footer_first'] + (bool) $page['footer_second'] + (bool) $page['footer_third']; ?>">
      <?php if($page['footer_first']): ?>
      <div class="footer-block"> <?php print render ($page['footer_first']); ?> </div>
      <?php endif; ?>
      <?php if($page['footer_second']): ?>
      <div class="footer-block"> <?php print render ($page['footer_second']); ?> </div>
      <?php endif; ?>
      <?php if($page['footer_third']): ?>
      <div class="footer-block"> <?php print render ($page['footer_third']); ?> </div>
      <?php endif; ?>
    </div>
    <?php endif; ?>
    <?php print render($page['footer']); ?> </div>
  <div id="bottom" class="clearfix">
    <div class="copyright"><?php print t('Copyright'); ?> &copy; <?php echo date("Y"); ?>, <a href="<?php print $front_page; ?>"><?php print $site_name; ?></a> <?php print t('Theme by'); ?> <a href="http://www.hydwebdesigner.com" target="_blank">Paramesh Ragala</a></div>
  </div>
</footer>
