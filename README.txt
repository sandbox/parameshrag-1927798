About Responsive responsive_personal Blog
==============================
Responsive responsive_personal Blog is a Drupal 7 theme. The theme is not dependent on any core theme. Its very light weight for fast loading with web 2.0 standards and modern look.
  Simple and clean design
  Drupal standards compliant
  Implementation of a JS Slideshow
  Multi-level drop-down menus
  Footer with 4 regions
  A total of 10 regions
  Compatible and tested on IE7, IE8, IE9+, Opera, Firefox, Chrome browsers

Browser compatibility:
======================
The theme has been tested on following browsers. IE7+, Firefox, Google Chrome, Opera.

Drupal compatibility:
=====================
This theme is compatible with Drupal 7.x.x

Developed by
============
Paramesh Ragala - www.hydwebdesigner.com

Slideshow 
=====================
To update the slideshow content, link and images go to Apprearance -> Theme Settings and edit the content in slide 1, slide 2, slide 3 and slide 4 forms.

Rounded Corners 
=====================
!important - You must enable clean URLs  to see the rounded corners for main menu, titles, blocks and buttons etc.. 

Social Icons 
=====================
Go to Apprearance -> Theme Settings and edit the solial network URLs, leave blank to hide.

