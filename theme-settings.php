<?php
/**
 * Implements hook_form_FORM_ID_alter().
 *
 * @responsive_personal_blog $form
 *   The form.
 * @responsive_personal_blog $form_state
 *   The form state.
 */
function responsive_personal_blog_form_system_theme_settings_alter(&$form, &$form_state) {
  $form['responsive_personal_blog_settings']['socialicon'] = array(
    '#type' => 'fieldset',
    '#title' => t('Social Network Links'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['responsive_personal_blog_settings']['socialicon']['socialicon_display'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show Social Icon'),
    '#default_value' => theme_get_setting('socialicon_display','responsive_personal_blog'),
    '#description'   => t("Check this option to show Social Icon. Uncheck to hide."),
  );
  $form['responsive_personal_blog_settings']['socialicon']['twitter_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Twitter Profile URL'),
    '#default_value' => theme_get_setting('twitter_url', 'responsive_personal_blog'),
    '#description'   => t("Enter your Twitter Profile URL. Leave blank to hide."),
  );
  $form['responsive_personal_blog_settings']['socialicon']['facebook_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Facebook Profile URL'),
    '#default_value' => theme_get_setting('facebook_url', 'responsive_personal_blog'),
    '#description'   => t("Enter your Facebook Profile URL. Leave blank to hide."),
  );
  $form['responsive_personal_blog_settings']['socialicon']['delicious_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Delicious Address'),
    '#default_value' => theme_get_setting('delicious_url', 'responsive_personal_blog'),
    '#description'   => t("Enter your Delicious URL. Leave blank to hide."),
  );
  $form['responsive_personal_blog_settings']['socialicon']['pinterest_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Pinterest Address'),
    '#default_value' => theme_get_setting('pinterest_url', 'responsive_personal_blog'),
    '#description'   => t("Enter your Pinterest URL. Leave blank to hide."),
  );
  
  
  
  /*** Front Page Slideshow ***/
  
  $form['responsive_personal_blog_settings']['slideshow'] = array(
    '#type' => 'fieldset',
    '#title' => t('Front Page Slideshow'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['responsive_personal_blog_settings']['slideshow']['slideshow_display'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show slideshow'),
    '#default_value' => theme_get_setting('slideshow_display','responsive_personal_blog'),
    '#description'   => t("Check this option to show Slideshow in front page. Uncheck to hide."),
  );
  $form['responsive_personal_blog_settings']['slideshow']['slide'] = array(
    '#markup' => t('You can change the description and URL of each slide in the following Slide Setting fieldsets.'),
  );
  
  
  $form['responsive_personal_blog_settings']['slideshow']['slide1'] = array(
    '#type' => 'fieldset',
    '#title' => t('Slide 1'),
  );
  $form['responsive_personal_blog_settings']['slideshow']['slide1']['slide1_desc'] = array(
    '#type' => 'textarea',
    '#title' => t('Slide Description'),
    '#default_value' => theme_get_setting('slide1_desc','responsive_personal_blog'),
  );
  $form['responsive_personal_blog_settings']['slideshow']['slide1']['slide1_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Slide URL'),
    '#default_value' => theme_get_setting('slide1_url','responsive_personal_blog'),
  );
  
  
  $form['responsive_personal_blog_settings']['slideshow']['slide2'] = array(
    '#type' => 'fieldset',
    '#title' => t('Slide 2'),
  );
  $form['responsive_personal_blog_settings']['slideshow']['slide2']['slide2_desc'] = array(
    '#type' => 'textarea',
    '#title' => t('Slide Description'),
    '#default_value' => theme_get_setting('slide2_desc','responsive_personal_blog'),
  );
  $form['responsive_personal_blog_settings']['slideshow']['slide2']['slide2_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Slide URL'),
    '#default_value' => theme_get_setting('slide2_url','responsive_personal_blog'),
  );
  $form['responsive_personal_blog_settings']['slideshow']['slide3'] = array(
    '#type' => 'fieldset',
    '#title' => t('Slide 3'),
  );
  $form['responsive_personal_blog_settings']['slideshow']['slide3']['slide3_desc'] = array(
    '#type' => 'textarea',
    '#title' => t('Slide Description'),
    '#default_value' => theme_get_setting('slide3_desc','responsive_personal_blog'),
  );
  $form['responsive_personal_blog_settings']['slideshow']['slide3']['slide3_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Slide URL'),
    '#default_value' => theme_get_setting('slide3_url','responsive_personal_blog'),
  );


  $form['responsive_personal_blog_settings']['slideshow']['slide4'] = array(
    '#type' => 'fieldset',
    '#title' => t('Slide 4'),
  );
  $form['responsive_personal_blog_settings']['slideshow']['slide4']['slide4_desc'] = array(
    '#type' => 'textarea',
    '#title' => t('Slide Description'),
    '#default_value' => theme_get_setting('slide4_desc','responsive_personal_blog'),
  );
  $form['responsive_personal_blog_settings']['slideshow']['slide4']['slide4_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Slide URL'),
    '#default_value' => theme_get_setting('slide4_url','responsive_personal_blog'),
  );
  
  
  $form['responsive_personal_blog_settings']['slideshow']['slideimage'] = array(
    '#markup' => t('To change the Slide Images, Replace the slide-01.png, slide-02.png, slide-03.png and slide-04.png in the images folder of the theme folder.'),
  );
}
